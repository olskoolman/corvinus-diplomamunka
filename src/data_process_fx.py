def add_data_element(response_data, data_key, data_value):
    updated_response_data = []
    for data in response_data:
        data[data_key] = data_value
        updated_response_data.append(data)
    return updated_response_data