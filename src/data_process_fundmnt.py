import requests_alphaventage as alphaventage
from datetime import datetime
import db_queries as db_query
import db_connect as db
import data_collector as collector

def alphaventage_fundmnt(instrument_symbol):
    source_symbol = instrument_symbol[0]
    instrument_symbol = instrument_symbol[1]
    eps = alphaventage.get_earnings(source_symbol)
    eps = alphaventage_process_eps(eps)
    eps_data = process_fundmnt_data(eps, instrument_symbol, 'EPS')
    cash_flow_data = alphaventage.get_cashflow_data(source_symbol)
    earnings_data, divident_data = alphaventage_process_cash_flow(cash_flow_data)
    earnings_data = process_fundmnt_data(earnings_data, instrument_symbol, 'Earnings')
    divident_data = process_fundmnt_data(divident_data, instrument_symbol, 'DIV')
    for write_data in [eps_data, earnings_data, divident_data]:
        for write in write_data:
            db_connection = db.connect()
            fundmnt_data_type = write[3]
            existing_data = db_query.check_existing_fundmnt_data(instrument_symbol, fundmnt_data_type)
            data_exists = collector.check_existing_data(existing_data, write[0])
            if not data_exists:
                db_query.write_fundmnt_data(write, db_connection)
            else:
                print("===> FUNDMNT data already exists")
            db_connection.commit()
            db.close_connection(db_connection = db_connection)

def alphaventage_process_cash_flow(cash_flow_data):
    divident_data = []
    earnings_data = []
    for cash_flow_data_key in cash_flow_data.keys():
        if 'annualReports' in cash_flow_data_key:
            cash_flow_data = cash_flow_data[cash_flow_data_key]
            for data in cash_flow_data:
                div_data = []
                earn_data = []
                cash_flow_date = ""
                earning_value = ""
                div_value = ""
                for data_keys in data.keys():
                    match data_keys:
                        case 'fiscalDateEnding':
                            cash_flow_date = datetime.strptime(data[data_keys], '%Y-%m-%d')
                        case 'netIncome':
                            earning_value = float(data[data_keys])
                        case 'dividendPayout':
                            div_value = float(data[data_keys])
                div_data.append(cash_flow_date)
                div_data.append(div_value)
                earn_data.append(cash_flow_date)
                earn_data.append(earning_value)
                divident_data.append(div_data)
                earnings_data.append(earn_data)
    return earnings_data, divident_data

def alphaventage_process_eps(raw_eps_data):
    eps_data = []
    for raw_eps_data_key in raw_eps_data.keys():
        if 'quarterlyEarnings' in raw_eps_data_key:
            raw_eps_data = raw_eps_data[raw_eps_data_key]
            for data in raw_eps_data:
                single_eps_data = []
                earning_data_date = datetime.strptime(data['reportedDate'], '%Y-%m-%d')
                earning_data_value = float(data['reportedEPS'])
                single_eps_data.append(earning_data_date)
                single_eps_data.append(earning_data_value)
                eps_data.append(single_eps_data)
    return eps_data

def fmprep_fundmnt(instrument_symbol):
    eps = fmprep.get_earnings(instrument_symbol)
    eps = alphaventage_process_eps(eps)
    eps_data = process_fundmnt_data(eps, instrument_symbol, 'EPS')
    cash_flow_data = alphaventage.get_cashflow_data(instrument_symbol)
    earnings_data, divident_data = alphaventage_process_cash_flow(cash_flow_data)
    earnings_data = process_fundmnt_data(earnings_data, instrument_symbol, 'Earnings')
    divident_data = process_fundmnt_data(divident_data, instrument_symbol, 'DIV')
    for write_data in [eps_data, earnings_data, divident_data]:
        for write in write_data:
            db_connection = db.connect()
            fundmnt_data_type = write[3]
            existing_data = db_query.check_existing_fundmnt_data(instrument_symbol, fundmnt_data_type)
            data_exists = collector.check_existing_data(existing_data, write[0])
            if not data_exists:
                db_query.write_fundmnt_data(write, db_connection)
            else:
                print("===> FUNDMNT data already exists")
            db_connection.commit()
            db.close_connection(db_connection = db_connection)

def process_fundmnt_data(all_fundmnt_data, instrument_symbol, data_type):
    extended_data = []
    for fundmnt_data in all_fundmnt_data:
        fundmnt_data.append(instrument_symbol)
        fundmnt_data.append(data_type)
        extended_data.append(fundmnt_data)
    return extended_data