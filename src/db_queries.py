import psycopg2
import db_connect as db

def execute_query(select_query, query_variables = []):
    db_connection = db.connect()
    select_cursor = db_connection.cursor()
    select_cursor.execute(select_query, query_variables)
    query_results = select_cursor.fetchall()
    db.close_connection(select_cursor, db_connection)
    return query_results

def write_tick_data(tick_data, db_connection):
    print(f"===> Write tick data: {tick_data}")
    insert_query = """
        INSERT INTO tick_data(tick_date, price_low, price_high, price_opening, price_closing, volume, time_frame, instrument)
        VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"""
    db_cursor = db_connection.cursor()
    db_cursor.execute(insert_query, tick_data)
    db_cursor.close()

def write_fundmnt_data(fundmnt_data, db_connection):
    print(f"===> Write tick data: {fundmnt_data}")
    insert_query = """
        INSERT INTO fundamental_data(data_date, data_value, instrument, data_type)
        VALUES (%s, %s, %s, %s);"""
    db_cursor = db_connection.cursor()
    db_cursor.execute(insert_query, fundmnt_data)
    db_cursor.close()

def check_existing_tick_data(instrument_symbol, time_frame):
    select_query = """
        SELECT TO_CHAR(tick_date, 'YYYY-MM-DD') as tick_date FROM tick_data
        WHERE instrument = %s AND time_frame = %s;"""
    query_variables = [instrument_symbol, time_frame]
    existing_data = execute_query(select_query, query_variables)
    return existing_data

def check_existing_fundmnt_data(instrument_symbol, time_frame):
    select_query = """
        SELECT TO_CHAR(data_date, 'YYYY-MM-DD') as fundamental_date FROM fundamental_data
        WHERE instrument = %s AND data_type = %s;"""
    query_variables = [instrument_symbol, time_frame]
    existing_data = execute_query(select_query, query_variables)
    return existing_data

def get_all_data_source():
    select_query = """
        SELECT data_source.instrument_symbol, data_source.source_symbol, data_source.source_name, data_source.data_type, instrument.stock_type FROM data_source
        INNER JOIN instrument
        ON instrument.short_name = data_source.instrument_symbol; """
    data_sources = execute_query(select_query)
    return data_sources

def get_dd_tick_data(instrument_symbol):
    select_query = """
        SELECT tick_date, price_low, price_high, price_opening, price_closing, volume FROM tick_data
        WHERE time_frame = 'dd' and instrument = %s
        ORDER BY tick_date ASC;
    """
    query_variables = [instrument_symbol]
    dd_tick_data = execute_query(select_query, query_variables)
    return dd_tick_data

def check_instrument_exist(instrument_symbol):
    select_query = """
    SELECT DISTINCT short_name FROM instrument
    WHERE short_name = %s
    """
    query_variables = [instrument_symbol]
    instrument_check_result = execute_query(select_query, query_variables)
    return instrument_check_result

def crate_instrument(instrument_symbol):
    print(f"===> Create instrument: {instrument_symbol}")
    db_connection = db.connect()
    insert_query = """
        INSERT INTO instrument(instrument_name, short_name, stock_type, currency)
        VALUES (%s, %s, %s, %s);"""
    insert_values = []
    insert_values.append(instrument_symbol)
    insert_values.append(instrument_symbol)
    insert_values.append("STOCK")
    insert_values.append("HUF")
    db_cursor = db_connection.cursor()
    db_cursor.execute(insert_query, insert_values)
    db_connection.commit()
    db_cursor.close()
    db_connection.close()

def get_all_tick_instrument():
    select_query = """
        SELECT DISTINCT instrument_symbol FROM data_source
        WHERE data_type = 'TICK'; """
    data_sources = execute_query(select_query)
    return data_sources