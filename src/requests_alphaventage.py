import requests
import os
from datetime import datetime
import data_process_fundmnt as fundmnt
import data_process_fx as fx
import data_process_tick as tick

def stock_data(instrumen_symbols, stock_type):
    match stock_type:
        case "STOCK":
            daily_stock(instrumen_symbols)
        case "FX":
            daily_fx(instrumen_symbols)
        case "FUNDMNT":
            fundmnt.alphaventage_fundmnt(instrumen_symbols)

def daily_stock(instrumen_symbols):
    request_uri = "https://www.alphavantage.co/query"
    reqest_parameters = {
         "function": 'TIME_SERIES_DAILY',
         "symbol": instrumen_symbols[0],
         "outputsize": 'full',
         "apikey": os.environ['ALPHAVENTAGE_APIKEY']
    }
    request_response = requests.get(request_uri, params=reqest_parameters)
    response_data = get_data_with_date(request_response.json())
    tick.process_data(response_data, instrument_symbol[1])

def daily_fx(instrumen_symbols):
    split_symbol = instrumen_symbols[0].split(".")
    from_symbol = split_symbol[0]
    to_symbol = split_symbol[1]
    request_uri = "https://www.alphavantage.co/query"
    reqest_parameters = {
         "function": 'FX_DAILY',
         "from_symbol": from_symbol,
         "to_symbol": to_symbol,
         "outputsize": 'full',
         "apikey": os.environ['ALPHAVENTAGE_APIKEY']
    }
    request_response = requests.get(request_uri, params=reqest_parameters)
    response_data = get_data_with_date(request_response.json())
    response_data = fx.add_data_element(response_data, 'volume', 0)
    tick.process_data(response_data, instrumen_symbols[1])

def get_earnings(instrumen_symbols):
    request_uri = "https://www.alphavantage.co/query"
    reqest_parameters = {
         "function": 'EARNINGS',
         "symbol": instrumen_symbols[0],
         "apikey": os.environ['ALPHAVENTAGE_APIKEY']
    }
    request_response = requests.get(request_uri, params=reqest_parameters)
    return request_response.json()

def get_cashflow_data(instrumen_symbols):
    request_uri = "https://www.alphavantage.co/query"
    reqest_parameters = {
         "function": 'CASH_FLOW',
         "symbol": instrumen_symbols[0],
         "apikey": os.environ['ALPHAVENTAGE_APIKEY']
    }
    request_response = requests.get(request_uri, params=reqest_parameters)
    return request_response.json()

def get_data_with_date(full_response):
    response_data = []
    for full_response_key in full_response.keys():
        if not full_response_key == 'Meta Data':
            full_response_data = full_response[full_response_key]
            for full_response_data_key in full_response_data.keys():
                tick_date = datetime.strptime(full_response_data_key, '%Y-%m-%d')
                mod_tick_data = full_response_data[full_response_data_key]
                mod_tick_data.update({'date': tick_date})
                response_data.append(mod_tick_data)
    return response_data

# EPS ROE DIV EAT

if __name__ == "__main__":
    instrument_symbol = ['IBM']
    fundmnt_data = []
    stock_data(instrument_symbol, 'FUNDMNT')