import requests
import os
import data_process_tick as tick

def stock_data(instrumen_symbols, stock_type):
    print("Choose")
    match stock_type:
        case "STOCK":
           daily_stock(instrumen_symbols)

def daily_stock(instrumen_symbols):
    print("Request")
    base_uri = "https://eodhd.com/api/eod/"
    request_uri = base_uri + instrumen_symbols[0]
    reqest_parameters = {
         "fmt": "json",
         "period": "d",
         "api_token": os.environ['EODHD_APIKEY']
    }
    request_response = requests.get(request_uri, params=reqest_parameters)
    response_data = request_response.json()
    tick.process_data(response_data, instrumen_symbols[1])
