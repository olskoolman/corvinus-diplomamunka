from datetime import datetime
import db_connect as db
import db_queries as db_query
import data_collector as collector

def process_data(daily_data, instrument_symbol, time_frame='dd'):
    db_connection = db.connect()
    existing_data = db_query.check_existing_tick_data(instrument_symbol, time_frame)
    for raw_tick_data in daily_data:
        tick_data = parse_tick_data(raw_tick_data)
        data_exists = collector.check_existing_data(existing_data, tick_data[0])
        if not data_exists:
            tick_data.append(time_frame)
            tick_data.append(instrument_symbol)
            db_query.write_tick_data(tick_data, db_connection)
        else:
            print("===> Tick data already exists")
    db_connection.commit()
    db.close_connection(db_connection = db_connection)

def parse_tick_data(raw_tick_data, tick_date="failed", price_low="failed", price_high="failed", price_opening="failed", price_closing="failed", volume="failed"):
    for tick_data_key in raw_tick_data.keys():
        if "open" in str.lower(tick_data_key):
            price_opening = float(raw_tick_data[tick_data_key])
        elif "high" in str.lower(tick_data_key):
            price_high = float(raw_tick_data[tick_data_key])
        elif "low" in str.lower(tick_data_key):
            price_low = float(raw_tick_data[tick_data_key])
        elif "close" in str.lower(tick_data_key):
            price_closing = float(raw_tick_data[tick_data_key])
        elif "volume" in str.lower(tick_data_key):
            volume = int(raw_tick_data[tick_data_key])
        elif "date" in str.lower(tick_data_key):
            tick_date = raw_tick_data[tick_data_key]
            if isinstance(tick_date, str):
                tick_date = datetime.strptime(tick_date, '%Y-%m-%d')
                print("===> str to date")
    tick_data = [tick_date, price_low, price_high, price_opening, price_closing, volume]
    return tick_data