import yfinance as yf
import pandas as pd
import data_process_tick as tick

def stock_data(instrumen_symbols, stock_type):
    match stock_type:
        case "CMDT":
            daily_cmdt(instrumen_symbols)

def daily_cmdt(instrumen_symbols):
    ticker = yf.Ticker(instrumen_symbols[0])
    raw_daily_data = ticker.history(period = 'max', interval = '1d')
    daily_data = raw_daily_data.to_dict('index')
    get_data_with_date(daily_data, instrumen_symbols[1])

def get_data_with_date(full_response, instrument_symbol):
    response_data = []
    for full_response_key in full_response.keys():
        full_response_data = full_response[full_response_key]
        tick_date = pd.to_datetime(full_response_key)
        tick_date = tick_date.date()
        full_response_data.update({'date': tick_date})
        response_data.append(full_response_data)
    tick.process_data(response_data, instrument_symbol)

if __name__ == "__main__":
    instrument_symbol = ['IBM']
    stock_data(instrument_symbol, 'FUNDMNT')