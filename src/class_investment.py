import pandas as pd
import numpy as np

class Investments:
    def __init__(self):
        self.investments = []
        self.investments_average_return = 0
        self.investments_id = ""
        self.investments_buys = {}
        self.investments_sells = {}
    def add_investment(self, investment):
        self.investments.append(investment)
    def average_return(self):
        returns = []
        returns_period = []
        if len(self.investments) > 0:
            for investment in self.investments:
                returns.append(investment.investment_return)
                returns_period.append((investment.sell_date - investment.buy_date).days)
            self.investments_average_return = np.average(returns, weights=returns_period)
    def summarize_buys_and_sells(self):
        for investment in self.investments:
            self.investments_buys[investment.buy_date] = investment.buy_value
            self.investments_sells[investment.sell_date] = investment.sell_value

class Investment:
    def __init__(self, instrument_symbol = "", buy_volume = 1, buy_date = "", buy_value = 0, sell_date = "", sell_value = 0, investment_return = 0.0):
        self.symbol = instrument_symbol
        self.volume = buy_volume
        self.buy_date = buy_date
        self.buy_value = buy_value
        self.investment_return = investment_return
        self.sell_date = sell_date
        self.sell_value = sell_value
    def sell(self):
        self.investment_return = (self.sell_value - self.buy_value) / self.buy_value