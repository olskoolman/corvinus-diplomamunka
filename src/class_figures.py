import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
from pathlib import Path
import math
import plotly.io as pio
import accomp_tech as tech
import seaborn as sns

class Heatmap:
    def __init__(self):
        self.instrument_symbols = []
        self.heatmap_dataframes = []
    def draw(self, fig_to_file_name="teszt.png"):
        root_path = Path(__file__).resolve().parents[1]
        fig_to_file_path = Path(root_path, "images", fig_to_file_name)
        num_of_rows = math.ceil(len(self.instrument_symbols) / 2)
        fig = make_subplots(
            rows = num_of_rows, cols = 2,
            subplot_titles = self.instrument_symbols
        )
        row_num = 1
        col_num = 0
        for dataframe in self.heatmap_dataframes:
            col_num = col_num + 1
            if col_num == 3:
                col_num = 1
            fig.add_trace(
                go.Heatmap(
                    z=dataframe.values.tolist(),
                    x=dataframe.index.tolist(),
                    y=dataframe.columns.tolist(),
                    colorscale=[[0, '#f05200'],[1, '#ffffc9']],
                    text=dataframe.values.tolist(),
                    texttemplate="%{text}",
                    textfont={"size":12},
                    showscale=False
                ),
                row = row_num, col = col_num
            )
            if col_num == 2:
                row_num = row_num + 1
        pio.kaleido.scope.default_height = num_of_rows * 200
        fig.write_image(fig_to_file_path)

class Fig_MACD:
    def __init__(self):
        self.tickers = []
        self.trade_dates = []
        self.trade_returns = []

    def draw(self, fig_to_file_name="teszt.png"):
        root_path = Path(__file__).resolve().parents[1]
        fig_to_file_path = Path(root_path, "images", fig_to_file_name)
        fig = make_subplots(
            rows = 3, cols = 1,
            subplot_titles = [self.tickers[0].symbol, "MACD", "Returns"]
        )
        for ticker in self.tickers:
            fig.add_trace(
                go.Candlestick(
                    x=ticker.dd_data['tick_date'],
                        open=ticker.dd_data['price_opening'],
                        close=ticker.dd_data['price_closing'],
                        high=ticker.dd_data['price_high'],
                        low=ticker.dd_data['price_low'],
                        showlegend=False
                ),
                row = 1, col=1
            )
            fig.update_layout(xaxis_rangeslider_visible=False)
            fig.add_trace(
                go.Scatter(
                    x=ticker.dd_data['tick_date'],
                    y=ticker.dd_data['MACD'],
                    name="MACD"
                ),
                row = 2, col = 1
            )
            fig.add_trace(
                go.Scatter(
                    x=ticker.dd_data['tick_date'],
                    y=ticker.dd_data['MACD_SIGNAL'],
                    name="MACD Signal"
                ),
                row = 2, col = 1
            )
            fig.add_trace(
                go.Bar(
                    x=self.trade_dates,
                    y=self.trade_returns,
                    showlegend=False,
                    marker_color='rgb(249,47,2)',
                    marker_line_color='rgb(1,1,1)',
                    marker_line_width=1.5
                ),
                row = 3, col = 1
            )
        pio.kaleido.scope.default_height = 2770
        pio.kaleido.scope.default_width = 1900
        fig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)',plot_bgcolor = 'rgba(0,0,0,0)')
        fig.write_image(fig_to_file_path)

class Fig_RSI:
    def __init__(self):
        self.tickers = []
        self.rsi_period = []
        self.buy_dates = []
        self.sell_dates = []
        self.trade_returns = []

    def draw(self, fig_to_file_name="teszt.png"):
        root_path = Path(__file__).resolve().parents[1]
        fig_to_file_path = Path(root_path, "images", fig_to_file_name)
        fig = make_subplots(
            rows = 4, cols = 1,
            subplot_titles = [self.tickers[0].symbol, tech.get_column_name("rsi", self.rsi_period[0]), tech.get_column_name("rsi", self.rsi_period[1]), tech.get_column_name("rsi", self.rsi_period[2])]
        )
        fig.add_trace(
            go.Candlestick(
                x=self.tickers[0].dd_data['tick_date'],
                    open=self.tickers[0].dd_data['price_opening'],
                    close=self.tickers[0].dd_data['price_closing'],
                    high=self.tickers[0].dd_data['price_high'],
                    low=self.tickers[0].dd_data['price_low'],
                    showlegend=False
            ),
            row = 1, col=1
        )
        for element in range(len(self.rsi_period)):
            rsi_period_name = tech.get_column_name("rsi", self.rsi_period[element])
            fig.update_layout(xaxis_rangeslider_visible=False)
            fig.add_trace(
                go.Scatter(
                    x=self.tickers[element].dd_data['tick_date'],
                    y=self.tickers[element].dd_data[rsi_period_name],
                    showlegend=False
                ),
                row = element + 2, col = 1
            )
            fig.add_trace(
                go.Bar(
                    x=self.sell_dates[element],
                    y=self.trade_returns[element],
                    showlegend=False,
                    marker_color='rgb(249,47,2)',
                    marker_line_color='rgb(1,1,1)',
                    marker_line_width=1.5
                ),
                row = element + 2, col = 1
            )
            for sell_date in self.sell_dates[element]:
                fig.add_vline(x=sell_date, line_width=1, line_dash="dash", line_color="orange")
            for buy_date in self.buy_dates[element]:
                fig.add_vline(x=buy_date, line_width=1, line_dash="dash", line_color="green")
        fig.add_hline(y=0.3, line_dash = "dash", line_width = 3, line_color = "royalblue")
        fig.add_hline(y=0.7, line_dash = "dash", line_width = 3, line_color = "royalblue")
        pio.kaleido.scope.default_height = 2770
        pio.kaleido.scope.default_width = 1900
        fig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)',plot_bgcolor = 'rgba(0,0,0,0)')
        fig.write_image(fig_to_file_path)

class Fig_ROC:
    def __init__(self):
        self.tickers = []
        self.roc_period = []
        self.buy_dates = []
        self.sell_dates = []
        self.trade_returns = []

    def draw(self, fig_to_file_name="teszt.png"):
        root_path = Path(__file__).resolve().parents[1]
        fig_to_file_path = Path(root_path, "images", fig_to_file_name)
        fig = make_subplots(
            rows = len(self.roc_period) + 1, cols = 1,
            subplot_titles = [
                self.tickers[0].symbol,
                tech.get_column_name("roc", self.roc_period[0]),
                tech.get_column_name("roc", self.roc_period[1]),
                tech.get_column_name("rsi", self.roc_period[2]),
                tech.get_column_name("rsi", self.roc_period[3]),
                tech.get_column_name("rsi", self.roc_period[4])
                ]
        )
        fig.add_trace(
            go.Candlestick(
                x=self.tickers[0].dd_data['tick_date'],
                    open=self.tickers[0].dd_data['price_opening'],
                    close=self.tickers[0].dd_data['price_closing'],
                    high=self.tickers[0].dd_data['price_high'],
                    low=self.tickers[0].dd_data['price_low'],
                    showlegend=False
            ),
            row = 1, col=1
        )
        for element in range(len(self.roc_period)):
            roc_period_name = tech.get_column_name("roc", self.roc_period[element])
            fig.update_layout(xaxis_rangeslider_visible=False)
            fig.add_trace(
                go.Scatter(
                    x=self.tickers[element].dd_data['tick_date'],
                    y=self.tickers[element].dd_data[roc_period_name],
                    showlegend=False
                ),
                row = element + 2, col = 1
            )
            fig.add_trace(
                go.Bar(
                    x=self.sell_dates[element],
                    y=self.trade_returns[element],
                    showlegend=False,
                    marker_color='rgb(249,47,2)',
                    marker_line_color='rgb(1,1,1)',
                    marker_line_width=1.5
                ),
                row = element + 2, col = 1
            )
            for sell_date in self.sell_dates[element]:
                fig.add_vline(x=sell_date, line_width=1, line_dash="dash", line_color="orange")
            for buy_date in self.buy_dates[element]:
                fig.add_vline(x=buy_date, line_width=1, line_dash="dash", line_color="green")
        fig.add_hline(y=0, line_dash = "dash", line_width = 1, line_color = "black")
        pio.kaleido.scope.default_height = 2770
        pio.kaleido.scope.default_width = 1900
        fig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)',plot_bgcolor = 'rgba(0,0,0,0)')
        fig.write_image(fig_to_file_path)

class Fig_Stoch:
    def __init__(self):
        self.tickers = []
        self.stoch_period = []
        self.buy_dates = []
        self.sell_dates = []
        self.trade_returns = []

    def draw(self, fig_to_file_name="teszt.png"):
        root_path = Path(__file__).resolve().parents[1]
        fig_to_file_path = Path(root_path, "images", fig_to_file_name)
        fig = make_subplots(
            rows = len(self.stoch_period) + 1, cols = 1,
            subplot_titles = [
                self.tickers[0].symbol,
                str(self.stoch_period[0]),
                str(self.stoch_period[1]),
                str(self.stoch_period[2])
                ]
        )
        fig.add_trace(
            go.Candlestick(
                x=self.tickers[0].dd_data['tick_date'],
                    open=self.tickers[0].dd_data['price_opening'],
                    close=self.tickers[0].dd_data['price_closing'],
                    high=self.tickers[0].dd_data['price_high'],
                    low=self.tickers[0].dd_data['price_low'],
                    showlegend=False
            ),
            row = 1, col=1
        )
        for element in range(len(self.stoch_period)):
            self.k_column_name = tech.get_column_name("stoch_K", self.stoch_period[element][0])
            self.d_column_name = tech.get_column_name("stoch_D", self.stoch_period[element][0])
            fig.update_layout(xaxis_rangeslider_visible=False)
            fig.add_trace(
                go.Scatter(
                    x=self.tickers[element].dd_data['tick_date'],
                    y=self.tickers[element].dd_data[self.k_column_name],
                    showlegend=False,
                    name="%K",
                    line=dict(color='royalblue', width=3)
                ),
                row = element + 2, col = 1
            )
            fig.add_trace(
                go.Scatter(
                    x=self.tickers[element].dd_data['tick_date'],
                    y=self.tickers[element].dd_data[self.d_column_name],
                    showlegend=False,
                    name="%D",
                    line=dict(color='firebrick', width=2, dash='dash')
                ),
                row = element + 2, col = 1
            )
            fig.add_trace(
                go.Bar(
                    x=self.sell_dates[element],
                    y=self.trade_returns[element],
                    showlegend=False,
                    marker_color='rgb(249,47,2)',
                    marker_line_color='rgb(1,1,1)',
                    marker_line_width=1.5
                ),
                row = element + 2, col = 1
            )
        fig.add_hline(y=0, line_dash = "dash", line_width = 1, line_color = "black")
        pio.kaleido.scope.default_height = 2770
        pio.kaleido.scope.default_width = 1900
        fig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)',plot_bgcolor = 'rgba(0,0,0,0)')
        fig.write_image(fig_to_file_path)

class Fig_Decision_Tree:
    def __init__(self):
        self.tickers = []
        self.fig_train_symbol = ""
        self.return_datas = pd.DataFrame()
        self.tradecount_datas = pd.DataFrame()
        self.decision_target_type = ""
        self.graphs_in_fig = 3
        self.fig_to_file_name = "teszt"

    def draw(self):
        root_path = Path(__file__).resolve().parents[1]        
        colors = sns.color_palette(None, len(self.tradecount_datas.columns))

        max_returndatas = float((self.return_datas.values).max())
        min_returndatas = float((self.return_datas.values).min())
        max_tradecount = float((self.tradecount_datas.values).max())
        if min_returndatas == 0 or max_returndatas == 0:
            min_tradecount = 0
        else:
            min_tradecount = max_tradecount / max_returndatas * min_returndatas

        graphs_together = self.graphs_in_fig
        self.return_datas.replace(0.0, None, inplace=True)
        self.tradecount_datas.replace(0.0, None, inplace=True)
        target_column_length_index = len(self.tradecount_datas.columns) - 1
        target_column_length_name = self.tradecount_datas.columns[target_column_length_index]
        rgb_red = int(colors[target_column_length_index][0] * 255)
        rgb_green = int(colors[target_column_length_index][1] * 255)
        rgb_blue = int(colors[target_column_length_index][2] * 255)
        target_column_length_line_color = "#{:02x}{:02x}{:02x}".format(rgb_red, rgb_green, rgb_blue)
        graph_counter = 1
        file_counter = 1

        for column_index in range(len(self.return_datas.columns)):
            if graph_counter == 1:
                fig = make_subplots(
                    subplot_titles=[self.fig_train_symbol],
                    specs=[[{"secondary_y": True}]]
                )
            if graph_counter <= graphs_together:
                rgb_red = int(colors[column_index][0] * 255)
                rgb_green = int(colors[column_index][1] * 255)
                rgb_blue = int(colors[column_index][2] * 255)
                line_color = "#{:02x}{:02x}{:02x}".format(rgb_red, rgb_green, rgb_blue)
                count_column_name = self.tradecount_datas.columns[column_index]
                return_column_name = self.return_datas.columns[column_index]

                fig.add_trace(
                    go.Scatter(
                        x = self.tradecount_datas.index,
                        y = self.tradecount_datas[count_column_name],
                        mode = 'lines+markers',
                        line = dict(
                            width = 1,
                            color = line_color
                        ),
                        name = count_column_name
                    ),
                    secondary_y = False
                )
                
                fig.add_trace(
                    go.Scatter(
                        x = self.return_datas.index,
                        y = self.return_datas[return_column_name],
                        mode = 'lines+markers',
                        line = dict(
                            width = 1,
                            color = line_color,
                            dash = 'dash'
                        ),
                        name = return_column_name
                    ),
                    secondary_y = True
                )
                graph_counter = graph_counter + 1

            if graph_counter > graphs_together:
                graph_counter = 1
                
                fig.add_trace(
                    go.Scatter(
                        x = self.tradecount_datas.index,
                        y = self.tradecount_datas[target_column_length_name],
                        mode = 'lines+markers',
                        line = dict(
                            width = 1,
                            color = target_column_length_line_color
                        ),
                        name = target_column_length_name
                    ),
                    secondary_y = False
                )
                fig.update_yaxes(range=[min_tradecount * 1.05, max_tradecount * 1.05], title_text="Trade Count", row=1, col=1)
                fig.update_yaxes(range=[min_returndatas * 1.05, max_returndatas * 1.05], title_text="Trade Returns", row=1, col=1, secondary_y = True)
                fig.add_hline(y=0, line_width = 1, line_color = "grey")
                fig.update_layout(
                    width=2000,
                    margin=dict(l=50, r=50, t=100, b=50)
                )
                fig.update_layout(paper_bgcolor = 'rgba(0,0,0,0)',plot_bgcolor = 'rgba(0,0,0,0)')
                fig_to_file_name_with_count = self.fig_to_file_name + "_" + str(file_counter)
                fig_to_file_path = Path(root_path, "images", fig_to_file_name_with_count)
                fig.write_image(fig_to_file_path, format='png')
                file_counter += 1

            # if column_index <= len(self.return_datas.columns) - 1:
            #     column_name_return = self.return_datas.columns[column_index]
            #     fig.add_trace(
            #         go.Bar(
            #             x = self.return_datas.index,
            #             y = self.return_datas[column_name_return],
            #             marker_color = line_color,
            #             name = column_name_return
            #         ),
            #     secondary_y = True
            #     )
