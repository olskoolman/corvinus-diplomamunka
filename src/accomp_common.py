import class_investment as classInvestment
from pathlib import Path
import regex as re

def trade_indexes(dd_data, column_name):
    trade_indexes = []
    open_indexes = dd_data.index[(dd_data[column_name] == 'open') | (dd_data[column_name] == 1)].tolist()
    close_indexes = dd_data.index[(dd_data[column_name] == 'close') | (dd_data[column_name] == 2)].tolist()
    for open in open_indexes:
        trade = []
        trade.append(open)
        for close in close_indexes:
            if close > open:
                trade.append(close)
                trade_indexes.append(trade)
                close_indexes.remove(close)
                break
            elif close <= open:
                close_indexes.remove(close)
    return trade_indexes

def trade_multiple_investment(dd_data, investments, trade_indexes, investment_symbol):
    for trade in trade_indexes:
        investment = classInvestment.Investment()
        investment.symbol = investment_symbol
        investment.buy_date  = dd_data.loc[trade[0], 'tick_date']
        investment.buy_value = dd_data.loc[trade[0], 'price_opening']
        print(f'#### {investment_symbol} == {"opened"} == {investment.buy_date} == {investment.buy_value }')
        investment.sell_date = dd_data.loc[trade[1], 'tick_date']
        investment.sell_value = dd_data.loc[trade[1], 'price_opening']
        investment.sell()
        investments.add_investment(investment)
        print(f'#### {investment_symbol} == {"closed"} == {investment.sell_date} == {investment.sell_value }')
    return investments

def trade_single_investment(dd_data, trade_indexes, investment_symbol):
    investment = classInvestment.Investment()
    investment.symbol = investment_symbol
    investment.buy_date  = dd_data.loc[trade_indexes[0], 'tick_date']
    investment.buy_value = dd_data.loc[trade_indexes[0], 'price_opening']
    print(f'#### {investment_symbol} == {"opened"} == {investment.buy_date} == {investment.buy_value }')
    investment.sell_date = dd_data.loc[trade_indexes[1], 'tick_date']
    investment.sell_value = dd_data.loc[trade_indexes[1], 'price_opening']
    investment.sell()
    print(f'#### {investment_symbol} == {"closed"} == {investment.sell_date} == {investment.sell_value }')
    return investment

def get_column_name(name_selector, name_data = ""):
    column_name = "NONE"
    match name_selector:
        case "SMA":
            column_name = "SMA-" + str(name_data)
        case "EMA":
            column_name = "EMA-" + str(name_data)
        case "rsi":
            column_name = "rsi-" + str(name_data)
        case "stoch_K":
            column_name = "stoch_K-" + str(name_data)
        case "stoch_D":
            column_name = "stoch_D-" + str(name_data)
    return column_name

def df_to_tex(dataframe, tex_name):
    root_path = Path(__file__).resolve().parents[1]
    tex_file_path = Path(root_path, "raw_tables", tex_name)
    tex_table = df_get_tex_content(dataframe)
    with open(tex_file_path, 'w') as tf:
        tf.write(tex_table)

def df_to_tex_heatmap(dataframe, tex_name):
    root_path = Path(__file__).resolve().parents[1]
    tex_file_path = Path(root_path, "raw_tables", tex_name)
    colspec = "    colspec = {X[1.8,l] *{" + str(len(dataframe.columns)) + "}{X}},\n"
    tex_table = df_get_tex_content(dataframe)
    tex_header = "\\begin{longtblr}{\n"
    tex_header += colspec
    tex_header += "    vlines, hlines,\n"
    tex_header += "    row{1} = {bg=gray,fg=black,halign=c},\n"
    tex_header += "    columns = {valign=m},\n"
    tex_header += "    rowhead = 1,\n    "
    min_val = dataframe.min().min()
    max_val = dataframe.max().max()
    for row in range(len(dataframe.index)):
        for column in range(len(dataframe.columns)):
            row_to_tex = row + 2
            column_to_tex = column + 2
            if (column == len(dataframe.columns) - 1) and (row == len(dataframe.index) - 1):
                bg_color = calculate_gradient_color(dataframe.iloc[row, column], min_val, max_val)
                tex_header += "cell{"+ str(row_to_tex) +"}{"+ str(column_to_tex) +"} = {bg="+ str(bg_color) +"}\n"
                tex_header += "}\n"
            else:
                bg_color = calculate_gradient_color(dataframe.iloc[row, column], min_val, max_val)
                tex_header += "cell{"+ str(row_to_tex) +"}{"+ str(column_to_tex) +"} = {bg="+ str(bg_color) +"}, "
    tex_foot = "\n\end{longtblr}"
    tex_full_table = tex_header + tex_table + tex_foot
    with open(tex_file_path, 'w') as tf:
        tf.write(tex_full_table)

def df_get_tex_content(dataframe):
    dataframe = dataframe.round(3)
    tex_raw_table = dataframe.style.to_latex()
    tex_table = '\n'.join(tex_raw_table.split('\n')[1:-2])
    tex_table = tex_table.replace("_", "\_")
    #tex_table = tex_table.replace(".000000", "")
    #tex_table = re.sub(r'.0{1,}\s&', ' &', tex_table)
    #tex_table = re.sub(r'0{2,}\s', ' ', tex_table)
    return tex_table

def calculate_gradient_color(value, min_val, max_val):
    if value == 0:
        return "white"
    elif value > 0:
        positive_value = int(100 * (value / max_val))
        return f"cyan!{positive_value}"
    else:
        negative_value = abs(int(100 * (value / min_val)))
        return f"orange!{negative_value}"
