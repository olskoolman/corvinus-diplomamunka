import psycopg2
import os
from configparser import ConfigParser
from pathlib import Path

def connect():
    db_connection = None
    config_file_section = "postgres-thesis"
    config_file_root = Path(__file__).resolve().parents[0]
    config_file_path = Path(config_file_root, "db_connection.ini")
    db_info = {}
    if os.path.exists(config_file_path):
        parser = ConfigParser()
        parser.read(config_file_path)
        if parser.has_section(config_file_section):
            key_values = parser.items(config_file_section)
            for item in key_values:
                db_info[item[0]] = item[1]
    else:
        db_info["host"] = os.environ['PSQL_HOST'],
        db_info["database"] = os.environ['PSQL_DATABASE'],
        db_info["user"] = os.environ['PSQL_USER'],
        db_info["password"] = os.environ['PSQL_PASSWORD']
    try:
        db_connection = psycopg2.connect(**db_info)
    except (Exception, psycopg2.DatabaseError) as error:
        print(error)
    return db_connection

def close_connection(db_cursor = None, db_connection = None):
    if db_cursor is not None:
        db_cursor.close()
    if db_connection is not None:
        db_connection.close()

if __name__ == "__main__":
    db_connection = connect()
    cur = db_connection.cursor()
    print('PostgreSQL database version:')
    cur.execute('SELECT version()')
    db_version = cur.fetchone()
    print(db_version)
    close_connection(cur, db_connection)
