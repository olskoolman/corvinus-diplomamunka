import pandas as pd
import os
import data_process_tick as tick
from pathlib import Path
from datetime import datetime
import math
import db_queries as db_quey

root_path = Path(__file__).resolve().parents[0]
csv_dir_path = Path(root_path, "csv_data", "premium")

for csv_file in os.listdir(csv_dir_path):
    csv_file_path = Path(csv_dir_path, csv_file)
    raw_csv_df = pd.read_csv(csv_file_path)
    daily_data = []
    instrument_symbol = ""
    for row in range(len(raw_csv_df) - 1):
        instrument_symbol = raw_csv_df.iloc[row, 0]
        if row + 1 > len(raw_csv_df):
            next_instrument_symbol = "end_of_datas"
        else:
            next_instrument_symbol = raw_csv_df.iloc[row + 1, 0]
        if math.isnan(raw_csv_df.iloc[row, 2]):
            continue
        raw_data = {}
        raw_data["open"] = raw_csv_df.iloc[row, 7]
        raw_data["high"] = raw_csv_df.iloc[row, 9]
        raw_data["low"] = raw_csv_df.iloc[row, 8]
        raw_data["close"] = raw_csv_df.iloc[row, 2]
        raw_data["volume"] = int(float(raw_csv_df.iloc[row, 3]))
        raw_data["date"] = datetime.strptime(raw_csv_df.iloc[row, 1], '%Y.%m.%d.')
        daily_data.append(raw_data)
        if instrument_symbol != next_instrument_symbol:
            print(f'==> KIIR ==> {instrument_symbol}')
            instrument_symbol = instrument_symbol + ".BUD"
            instrument_check = db_quey.check_instrument_exist(instrument_symbol)
            print(instrument_check)
            if len(instrument_check) == 0:
                db_quey.crate_instrument(instrument_symbol)
            tick.process_data(daily_data, instrument_symbol)
            daily_data = []