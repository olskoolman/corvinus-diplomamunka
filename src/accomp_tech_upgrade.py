def sma(ticker, short_terms, mid_terms):
    for short_term in short_terms:
        ticker.tick_sma(short_term)
        for mid_term in mid_terms:
            ticker.tick_sma(mid_term)
            ticker.signals_add_ma(short_term, mid_term, "SMA")

def ema(ticker, short_terms, mid_terms):
    for short_term in short_terms:
        ticker.tick_ema(short_term)
        for mid_term in mid_terms:
            ticker.tick_ema(mid_term)
            ticker.signals_add_ma(short_term, mid_term, "EMA")

def macd(ticker, macd_line_low = 16, macd_line_high = 26, macd_signal = 9):
    macd_lines = []
    macd_lines.append(macd_line_low)
    macd_lines.append(macd_line_high)
    macd_lines.append(macd_signal)
    ticker.tick_macd(macd_line_low, macd_line_high, macd_signal)
    ticker.signals_add_macd(macd_lines)

def rsi(ticker, rsi_days = [9, 14, 28]):
    for period in rsi_days:
        ticker.tick_rsi(period)
        ticker.signals_add_rsi(period)

def roc(ticker, roc_days = [12, 25, 65, 130, 260]):
    for period in roc_days:
        ticker.tick_roc(period)
        ticker.signals_add_roc(period)

def stoch(ticker, stoch_periods = [14, 3]):
    for periods in stoch_periods:
        ticker.tick_stoch(periods)
        ticker.signals_add_stoch(periods)
