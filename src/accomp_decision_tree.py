import accomp_tech_upgrade as tech
import class_tick as classTick
import class_investment as classInvestment
import db_queries as db_query
import accomp_common as common
import class_figures as classFigure
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.tree import DecisionTreeRegressor

def define_target_by_tech(ticker, tech_type, short_period=0, mid_period=0, attribute_value=0):
    target_column = {}
    if (tech_type == 'MA' or tech_type == 'EMA'):
        signal_column_name = "SIGNAL_" + tech_type + "_" + str(short_period) + "_" + str(mid_period)
    else:
        signal_column_name = "SIGNAL_" + tech_type + "_" + str(attribute_value)
    trade_indexes = common.trade_indexes(ticker.dd_data, signal_column_name)
    for trade_index in trade_indexes:
        investment = common.trade_single_investment(ticker.dd_data, trade_index, ticker.symbol)
        if investment.investment_return > 0:
            target_column[trade_index[0]] = 1
            target_column[trade_index[1]] = 2
    print(f"#TARGET_COLUMN_VALUES# {target_column}")
    return target_column

def train_classification(ticker, target_column_name, feature_columns = ['price_low', 'price_high', 'price_opening', 'price_closing', 'volume']):
    X = ticker.dd_data[feature_columns]
    y = ticker.dd_data[target_column_name]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    classification = DecisionTreeClassifier()
    classification.fit(X_train, y_train)
    return classification

def train_regression(ticker, target_column_name, feature_columns = ['price_low', 'price_high', 'price_opening', 'price_closing', 'volume']):
    X = ticker.dd_data[feature_columns]
    y = ticker.dd_data[target_column_name]
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    regression = DecisionTreeRegressor(max_depth=3, random_state=42)
    regression.fit(X_train, y_train)
    return regression

def ml_ma_based(train_symbol, instrument_symbols, ma_short_terms, ma_mid_terms, decision_tree_type = "classification", create_figures=True):
    dataframe_dectree_ma = pd.DataFrame()
    dataframe_dectree_ma_tradecount = pd.DataFrame()
    axis_periods_texts = {}
    applied_ma_count = 0
    train_ticker = classTick.Tick(train_symbol)
    tech.ema(train_ticker, ma_short_terms, ma_mid_terms)
    target_column = {}
    for short_period in ma_short_terms:
        for mid_period in ma_mid_terms:
            target_column_name = "TARGET_" + "EMA" + "_" + str(short_period) + "_" + str(mid_period)
            current_target_column = define_target_by_tech(train_ticker, "EMA", short_period, mid_period)
            target_column = target_column | current_target_column
            train_ticker.dd_data[target_column_name] = pd.Series(target_column).astype(int)
            train_ticker.dd_data[target_column_name] = train_ticker.dd_data[target_column_name].fillna(0).astype(int)
            if decision_tree_type == "classification":
                trained_model = train_classification(train_ticker, target_column_name)
            if decision_tree_type == "regression":
                trained_model = train_regression(train_ticker, target_column_name)
            dataframe_dectree_ma, dataframe_dectree_ma_tradecount, investments = apply_classificaton(instrument_symbols, "EMA", trained_model, dataframe_dectree_ma, dataframe_dectree_ma_tradecount, applied_ma_count, short_period, mid_period)
            axis_periods_text = str(train_symbol) + "_EMA_NUM_" + str(applied_ma_count)
            axis_periods_texts[applied_ma_count] = axis_periods_text
            column_name_target_len = "TARGET_COLUMN_LEN"
            dataframe_dectree_ma_tradecount.at[applied_ma_count, column_name_target_len] = len(target_column)
            applied_ma_count = applied_ma_count + 1
    dataframe_dectree_ma = dataframe_dectree_ma.rename(index = axis_periods_texts)
    dataframe_dectree_ma_tradecount = dataframe_dectree_ma_tradecount.rename(index = axis_periods_texts)
    if create_figures:
        create_figure(dataframe_dectree_ma_tradecount, dataframe_dectree_ma, axis_periods_text, train_symbol, graphs_in_fig=3, decision_tree_type=decision_tree_type)
        create_tex_tables(dataframe_dectree_ma_tradecount, dataframe_dectree_ma, axis_periods_text, train_symbol, decision_tree_type)
    return dataframe_dectree_ma, dataframe_dectree_ma_tradecount

def ml_single_attribute_based(train_symbol, instrument_symbols, attribute_values, tech_type, decision_tree_type = "classification", create_figures=True):
    dataframe_dectree = pd.DataFrame()
    dataframe_dectree_tradecount = pd.DataFrame()
    axis_periods_texts = {}
    applied_attribute_count = 0
    train_ticker = classTick.Tick(train_symbol)
    train_ticker = apply_train_ticker(train_ticker, tech_type, attribute_values)
    target_column = {}
    for attribute_value in attribute_values:
        target_column_name = "TARGET_" + tech_type +  "_" + str(attribute_value)
        current_target_column = define_target_by_tech(train_ticker, tech_type, attribute_value=attribute_value)
        target_column = target_column | current_target_column
        train_ticker.dd_data[target_column_name] = pd.Series(target_column).astype(int)
        train_ticker.dd_data[target_column_name] = train_ticker.dd_data[target_column_name].fillna(0).astype(int)
        if decision_tree_type == "classification":
            trained_model = train_classification(train_ticker, target_column_name)
        if decision_tree_type == "regression":
            trained_model = train_regression(train_ticker, target_column_name)
        dataframe_dectree, dataframe_dectree_tradecount, investments = apply_classificaton(instrument_symbols, tech_type, trained_model, dataframe_dectree, dataframe_dectree_tradecount, applied_attribute_count, attribute_value=attribute_value)
        axis_periods_text = str(train_symbol) + "_" + tech_type + "_NUM_" + str(applied_attribute_count)
        axis_periods_texts[applied_attribute_count] = axis_periods_text
        column_name_target_len = "TARGET_COLUMN_LEN"
        dataframe_dectree_tradecount.at[applied_attribute_count, column_name_target_len] = len(target_column)
        applied_attribute_count = applied_attribute_count + 1
    dataframe_dectree = dataframe_dectree.rename(index = axis_periods_texts)
    dataframe_dectree_tradecount = dataframe_dectree_tradecount.rename(index = axis_periods_texts)
    if create_figures:
        create_figure(dataframe_dectree_tradecount, dataframe_dectree, axis_periods_text, train_symbol, graphs_in_fig=3, decision_tree_type=decision_tree_type)
        create_tex_tables(dataframe_dectree_tradecount, dataframe_dectree, axis_periods_text, train_symbol, decision_tree_type)
    return dataframe_dectree, dataframe_dectree_tradecount

def apply_train_ticker(train_ticker, tech_type, attribute_values):
    match tech_type:
        case "RSI":
            tech.rsi(train_ticker, attribute_values)
        case "ROC":
            tech.roc(train_ticker, attribute_values)
        case "STOCH":
            tech.stoch(train_ticker, attribute_values)
        case "MACD":
            tech.macd(train_ticker, attribute_values[0][0], attribute_values[0][1], attribute_values[0][2])
    return train_ticker

def apply_classificaton(instrument_symbols, tech_type, trained_model, dataframe_dectree_returns, dataframe_dectree_tradecount, applied_attribute_count, short_period=None, mid_period=None, attribute_value=None, feature_columns = ['price_low', 'price_high', 'price_opening', 'price_closing', 'volume']):
    for symbol in instrument_symbols:
        ticker = classTick.Tick(symbol)
        investments = classInvestment.Investments()
        if attribute_value is None:
            prediction_column_name = "PREDICTION_" + tech_type + "_" + str(short_period) + "_" + str(mid_period)
        else:
            prediction_column_name = "PREDICTION_" + tech_type + "_" + str(attribute_value)
        predicitons = trained_model.predict(ticker.dd_data[feature_columns])
        ticker.dd_data[prediction_column_name] = predicitons
        trade_indexes = common.trade_indexes(ticker.dd_data, prediction_column_name)
        # np.set_printoptions(threshold=sys.maxsize)
        # print(predicitons)
        # print(f"#TRADE_INDEXES# {trade_indexes}")
        investments = common.trade_multiple_investment(ticker.dd_data, investments, trade_indexes, ticker.symbol)
        investments.average_return()
        investments.summarize_buys_and_sells()
        column_name_trade_counts = symbol + "_TRADES"
        column_name_returns = symbol + "_RETURNS"
        dataframe_dectree_returns.at[applied_attribute_count, column_name_returns] = round(investments.investments_average_return, 3)
        dataframe_dectree_tradecount.at[applied_attribute_count, column_name_trade_counts] = len(trade_indexes)
    return dataframe_dectree_returns, dataframe_dectree_tradecount, investments

def create_figure(dataframe_count, dataframe_return, decision_target_type, train_symbol, graphs_in_fig=3, decision_tree_type = "classification"):
    figure_summary = classFigure.Fig_Decision_Tree()
    figure_summary.fig_train_symbol = "Train base symbol: " + str(train_symbol)
    figure_summary.return_datas = dataframe_return
    figure_summary.tradecount_datas = dataframe_count
    figure_summary.graphs_in_fig = graphs_in_fig
    figure_summary.fig_to_file_name = "ML_DecisionTree_" + decision_tree_type + "_" + str(decision_target_type) + "_" + str(train_symbol) + ".png"
    figure_summary.draw()

def create_tex_tables(dataframe_count, dataframe_return, decision_target_type, train_symbol, decision_tree_type = "classification"):
    if (not dataframe_return.empty) or (dataframe_return is not None):
        common.df_to_tex(dataframe_return, "ML_decision_tree_" + decision_tree_type + "_" + decision_target_type + "_return_" + train_symbol + ".tex")
    if (not dataframe_count.empty) or (dataframe_count is not None):
        common.df_to_tex(dataframe_count, "ML_decision_tree_" + decision_tree_type + "_" + decision_target_type + "_count_" + train_symbol + ".tex")

def create_max_returns_table(return_dataframes, decision_tree_type = "classification"):
    df_all_returns = pd.concat(return_dataframes, axis=0)
    df_all_returns = df_all_returns.fillna(0).astype(float)
    df_all_returns = df_all_returns.round(3)
    df_max_returns = pd.DataFrame(columns=df_all_returns.columns)
    for column in df_all_returns.columns:
        max_row_index = df_all_returns[column].idxmax()
        df_max_returns.loc[max_row_index] = df_all_returns.loc[max_row_index]
    common.df_to_tex_heatmap(df_max_returns, "ML_decision_tree_" + decision_tree_type +"_MAX_RETURNS.tex")
    print(df_max_returns)
    return df_max_returns

def create_max_count_table(df_max_returns, count_tables, decision_tree_type = "classification"):
    df_all_count = pd.concat(count_tables, axis=0)
    df_all_count = df_all_count.fillna(0).astype(float)
    df_max_count = pd.DataFrame(columns=df_all_count.columns)
    for row in df_max_returns.index:
        if row in df_all_count.index:
            df_max_count.loc[row] = df_all_count.loc[row].values
        else:
            print(f"Row '{row}' not found in df_all_count.")
            print(f"===> {df_max_count.loc[row]}")
    column_move_to_end = df_max_count.pop("TARGET_COLUMN_LEN")
    df_max_count["TARGET_COLUMN_LEN"] = column_move_to_end
    common.df_to_tex(df_max_count, "ML_decision_tree_" + decision_tree_type + "_COUNT.tex")
    print(df_max_count)

def remove_train_symbol(instrument_symbols_list, train_symbol):
    instruments = []
    for instrument in instrument_symbols_list:
        instruments.append(instrument[0])
    try:
        instruments.remove(train_symbol)
    except:
        print(f'{train_symbol} is not in list!')
    return instruments

def run_regression():
    decision_tree_type = "regression"
    teszt = False
    instrument_symbols = [("OTP.BUD",),("INTC",),("EUR.HUF",)]
    train_symbols = [("MSFT",)]
    raw_instruments = list(db_query.get_all_tick_instrument())
    ma_short_terms = [10, 15, 20, 25, 30]
    ma_mid_terms = [50, 65, 100, 130, 200]
    rsi_days = [9, 14, 28]
    roc_days = [12, 25, 65, 130, 260]
    stoch_periods = [[14, 3], [80, 3], [280, 3]]
    macd_lines = [[16, 26, 9],]
    return_tables = []
    count_tables = []

    if teszt is False:
        train_symbols = raw_instruments
        instrument_symbols = raw_instruments
    for train_symbol in train_symbols:
        train_symbol = train_symbol[0]
        instrument_symbols_list = instrument_symbols
        instrument_symbols_list = remove_train_symbol(instrument_symbols_list, train_symbol)
        ma_dataframe_return, ma_dataframe_count = ml_ma_based(train_symbol, instrument_symbols_list, ma_short_terms, ma_mid_terms, decision_tree_type)
        return_tables.append(ma_dataframe_return)
        count_tables.append(ma_dataframe_count)
        rsi_dataframe_return, rsi_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, rsi_days, "RSI", decision_tree_type)
        return_tables.append(rsi_dataframe_return)
        count_tables.append(rsi_dataframe_count)
        roc_dataframe_return, roc_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, roc_days, "ROC", decision_tree_type)
        return_tables.append(roc_dataframe_return)
        count_tables.append(roc_dataframe_count)
        stoch_dataframe_return, stoch_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, stoch_periods, "STOCH", decision_tree_type)
        return_tables.append(stoch_dataframe_return)
        count_tables.append(stoch_dataframe_count)
        macd_dataframe_return, macd_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, macd_lines, "MACD", decision_tree_type)
        return_tables.append(macd_dataframe_return)
        count_tables.append(macd_dataframe_count)
    df_max_returns = create_max_returns_table(return_tables, decision_tree_type)
    create_max_count_table(df_max_returns, count_tables, decision_tree_type)

def run_classification():
    decision_tree_type = "classification"
    teszt = False
    instrument_symbols = [("OTP.BUD",),("INTC",),("EUR.HUF",)]
    train_symbols = [("MSFT",)]
    raw_instruments = list(db_query.get_all_tick_instrument())
    ma_short_terms = [10, 15, 20, 25, 30]
    ma_mid_terms = [50, 65, 100, 130, 200]
    rsi_days = [9, 14, 28]
    roc_days = [12, 25, 65, 130, 260]
    stoch_periods = [[14, 3], [80, 3], [280, 3]]
    macd_lines = [[16, 26, 9],]
    return_tables = []
    count_tables = []

    if teszt is False:
        train_symbols = raw_instruments
        instrument_symbols = raw_instruments
    for train_symbol in train_symbols:
        train_symbol = train_symbol[0]
        instrument_symbols_list = instrument_symbols
        instrument_symbols_list = remove_train_symbol(instrument_symbols_list, train_symbol)
        ma_dataframe_return, ma_dataframe_count = ml_ma_based(train_symbol, instrument_symbols_list, ma_short_terms, ma_mid_terms, decision_tree_type)
        return_tables.append(ma_dataframe_return)
        count_tables.append(ma_dataframe_count)
        rsi_dataframe_return, rsi_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, rsi_days, "RSI", decision_tree_type)
        return_tables.append(rsi_dataframe_return)
        count_tables.append(rsi_dataframe_count)
        roc_dataframe_return, roc_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, roc_days, "ROC", decision_tree_type)
        return_tables.append(roc_dataframe_return)
        count_tables.append(roc_dataframe_count)
        stoch_dataframe_return, stoch_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, stoch_periods, "STOCH", decision_tree_type)
        return_tables.append(stoch_dataframe_return)
        count_tables.append(stoch_dataframe_count)
        macd_dataframe_return, macd_dataframe_count = ml_single_attribute_based(train_symbol, instrument_symbols_list, macd_lines, "MACD", decision_tree_type)
        return_tables.append(macd_dataframe_return)
        count_tables.append(macd_dataframe_count)
    df_max_returns = create_max_returns_table(return_tables, decision_tree_type)
    create_max_count_table(df_max_returns, count_tables, decision_tree_type)

if __name__ == "__main__":
    run_classification()
    run_regression()