import db_queries as db_query
import pandas as pd
import pandas_ta as ta
import accomp_common as common
import numpy as np

class Tick:
    def __init__(self, instrument_symbol):
        self.symbol = instrument_symbol
        query_data = db_query.get_dd_tick_data(instrument_symbol)
        self.dd_data = pd.DataFrame(query_data, columns=['tick_date', 'price_low', 'price_high', 'price_opening', 'price_closing', 'volume'])

    def tick_sma(self, ma_period, sma_base = 'price_closing'):
        column_name = common.get_column_name("SMA", ma_period)
        self.dd_data[column_name] = self.dd_data.loc[:, sma_base].rolling(window=ma_period).mean()
        replace_nones(column_name)
    
    def tick_ema(self, ma_period, ema_base = 'price_closing'):
        column_name = common.get_column_name("EMA", ma_period)
        self.dd_data[column_name] = self.dd_data.loc[:, ema_base].ewm(span = ma_period, adjust = False).mean()
        replace_nones(self, column_name)
    
    def tick_macd(self, ema_low, ema_high, ema_signal):
        column_name_macd = "MACD"
        column_name_macd_signal = "MACD_SIGNAL_LINE"
        column_low = common.get_column_name("EMA", ema_low)
        column_high = common.get_column_name("EMA", ema_high)
        column_signal = common.get_column_name("EMA", ema_signal)
        self.tick_ema(ema_low)
        self.tick_ema(ema_high)
        self.dd_data[column_name_macd] = self.dd_data[column_low] - self.dd_data[column_high]
        self.tick_ema(ema_signal, column_name_macd)
        self.dd_data[column_name_macd_signal] = self.dd_data[column_signal]
        replace_nones(self, column_name_macd)

    def tick_rsi(self, period):
        rsi_column_name = common.get_column_name("rsi", period)
        self.dd_data[rsi_column_name] = ta.rsi(self.dd_data['price_closing'], length = period, scalar = 1)
        replace_nones(self, rsi_column_name)

    def tick_roc(self, period):
        roc_column_name = common.get_column_name("roc", period)
        self.dd_data[roc_column_name] = ta.rsi(self.dd_data['price_closing'], length = period, scalar = 1)
        replace_nones(self, roc_column_name)

    def tick_stoch_builtin(self, period = [14,3]):
        k_period = period[0]
        d_period = period[1]
        self.dd_data.ta.stoch(high="price_high", low="price_low", close="price_closing", k=k_period, d=d_period, append=True)

    def tick_stoch(self, period = [14, 3]):
        k_period = period[0]
        d_period = period[1]
        k_column_name = common.get_column_name("stoch_K_", str(period))
        d_column_name = common.get_column_name("stoch_D_", str(period))
        self.dd_data['high_max'] = self.dd_data['price_high'].rolling(k_period).max()
        self.dd_data['low_min'] = self.dd_data['price_low'].rolling(k_period).min()
        closing_index = self.dd_data.columns.get_loc("price_closing")
        high_max_index = self.dd_data.columns.get_loc("price_high")
        low_min_index = self.dd_data.columns.get_loc("price_low")
        k_results = {}
        for data_row in range(len(self.dd_data)):
            current_close = self.dd_data.iloc[data_row, closing_index]
            period_min = self.dd_data.iloc[data_row, low_min_index]
            period_max = self.dd_data.iloc[data_row, high_max_index]
            if period_min == period_max:
                k_results[data_row] = 0
            else:
                k_results[data_row] = (current_close - period_min) * 100 / (period_max - period_min)
            if k_results[data_row] > 100:
                k_results[data_row] = 100
            elif k_results[data_row] < 0:
                k_results[data_row] = 0
        self.dd_data[k_column_name] = pd.Series(k_results)
        self.dd_data[d_column_name] = self.dd_data[k_column_name].rolling(window=d_period).mean()

    def current_trend(self, base_column_name = 'price_closing'):
        signal_column_name = "SIGNAL_TREND"
        base_column_index = self.dd_data.columns.get_loc(base_column_name)
        for row in range(len(self.dd_data) - 1):
            if self.dd_data.iloc[row, base_column_index] <= self.dd_data.iloc[row + 1, base_column_index]:
                self.dd_data.at[row, signal_column_name] = "incr"
        for row in range(len(self.dd_data) - 1):
            if self.dd_data.iloc[row, base_column_index] > self.dd_data.iloc[row + 1, base_column_index]:
                self.dd_data.at[row, signal_column_name] = "decr"

    def signals_add_ma(self, short_period, mid_period, ma_type):
        column_short = common.get_column_name(ma_type, short_period)
        column_mid = common.get_column_name(ma_type, mid_period)
        signal_columnn = "SIGNAL_" + ma_type + "_" + str(short_period) + "_" + str(mid_period)
        rules = [
            (self.dd_data[column_short] > self.dd_data[column_mid]) & (self.dd_data[column_short].shift(periods = 1) <= self.dd_data[column_mid].shift(periods = 1)),
            (self.dd_data[column_short] < self.dd_data[column_mid]) & (self.dd_data[column_short].shift(periods = 1) >= self.dd_data[column_mid].shift(periods = 1))
        ]
        choices = ['open', 'close']
        self.dd_data[signal_columnn] = np.select(rules, choices, default = 'hold')

    def signals_add_macd(self, macd_lines):
        column_name_macd = "MACD"
        column_name_macd_signal = "MACD_SIGNAL_LINE"
        signal_columnn = "SIGNAL_MACD_" + str(macd_lines)
        rules = [
            self.dd_data[column_name_macd] > self.dd_data[column_name_macd_signal],
            self.dd_data[column_name_macd] < self.dd_data[column_name_macd_signal],
            self.dd_data[column_name_macd] == self.dd_data[column_name_macd_signal]
        ]
        choices = ['open', 'close', 'hold']
        self.dd_data[signal_columnn] = np.select(rules, choices, default = 'hold')
        
    def signals_add_rsi(self, period):
        rsi_column_name = common.get_column_name("rsi", period)
        signal_column = "SIGNAL_RSI_" + str(period)
        rsi_value = self.dd_data[rsi_column_name]
        rsi_prev_value = self.dd_data[rsi_column_name].shift(1)
        rules = [
            (
                (rsi_value < 0.20) |
                ((rsi_value < 0.30) & (rsi_prev_value < rsi_value))
            ),
            (
                (rsi_value > 0.80) |
                ((rsi_value > 0.70) & (rsi_prev_value > rsi_value))
            ),
            (
                (rsi_value < 0.70) |
                (rsi_value > 0.30)
            )
        ]
        choices = ['open', 'close', 'hold']
        self.dd_data[signal_column] = np.select(rules, choices, default = 'hold')

    def signals_add_roc(self, period):
        roc_column_name = common.get_column_name("roc", period)
        signal_column = "SIGNAL_ROC_" + str(period) 
        max_value = self.dd_data[roc_column_name].max()
        min_value = self.dd_data[roc_column_name].min()
        upper_limit = ((max_value - 0.5) * 0.7) + 0.5
        lower_limit = 0.5 - ((0.5 - min_value) * 0.7)
        rules = [
            (
                self.dd_data[roc_column_name] < lower_limit
            ),
            (
                self.dd_data[roc_column_name] > upper_limit
            ),
            (
                (self.dd_data[roc_column_name] == upper_limit) |
                (self.dd_data[roc_column_name] == lower_limit)
            )
        ]
        choices = ['open', 'close', 'hold']
        self.dd_data[signal_column] = np.select(rules, choices, default = 'hold')


    def signals_add_stoch(self, period): 
        k_column_name = common.get_column_name("stoch_K_", str(period))
        d_column_name = common.get_column_name("stoch_D_", str(period))
        signal_columnn = "SIGNAL_STOCH_" + str(period)
        k_value = self.dd_data[k_column_name]
        d_value = self.dd_data[d_column_name]
        k_prev_value = self.dd_data[k_column_name].shift(1)
        d_prev_value = self.dd_data[d_column_name].shift(1)
        rules = [
            (
                (k_value > 20) & (k_value > k_prev_value) |
                (d_value > 20) & (d_value > d_prev_value) |
                (k_value > d_value) |
                (d_value < 15)
            ),
            (
                (k_value < 80) & (k_value < k_prev_value) |
                (d_value < 80) & (d_value < d_prev_value) |
                (k_value < d_value) |
                (d_value > 85)
            ),
            (
                (k_value == d_value) |
                ((d_value < 85) & (d_value > 15))
            )
        ]
        choices = ['open', 'close', 'hold']
        self.dd_data[signal_columnn] = np.select(rules, choices, default = 'hold')

def replace_nones(self, column_name):
    self.dd_data[column_name].fillna(0).astype(int)
