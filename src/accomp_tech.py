import pandas as pd
import class_investment as classInv
import class_tick as classTick
import class_figures as classFigure
import db_queries as db_query
from pathlib import Path
import math
import accomp_common as common

def get_sma_return(dd_data, short_term, mid_term, ma_type, investment_symbol = ""):
    column_short = get_column_name(ma_type, short_term)
    column_mid = get_column_name(ma_type, mid_term)
    investments = classInv.Investments()
    investment = classInv.Investment()
    for data_row in range(len(dd_data) - 1):
        if dd_data.loc[data_row, column_short] > dd_data.loc[data_row, column_mid] and investment.buy_value == 0:
            investment = trade(dd_data, investment, investment_symbol, data_row, "buy")
        elif dd_data.loc[data_row, column_short] < dd_data.loc[data_row, column_mid] and investment.buy_value != 0:
            investments, investment = trade(dd_data, investment, investments, investment_symbol, data_row, "sell")
    ma_return_weighted_avg = investments.average_return()
    print(f'ma return ={column_short}={column_mid}== {ma_return_weighted_avg}')
    return ma_return_weighted_avg

def moving_avrage(short_terms, mid_terms, instruments, ma_type):
    ma_heatmap = classFigure.Heatmap()
    for instrument in instruments:
        ticker = classTick.Tick(instrument)
        row_names = []
        df_ma_returns = pd.DataFrame()
        for mid in mid_terms:
            row_names.append(get_column_name(ma_type, mid))
            if ma_type == "SMA":
                ticker.tick_sma(mid)
            elif ma_type == "EMA":
                ticker.tick_ema(mid)
        for short in short_terms:
            if ma_type == "SMA":
                ticker.tick_sma(short)
            elif ma_type == "EMA":
                ticker.tick_ema(short)
        df_ma_returns.set_axis(row_names)
        print(df_ma_returns)
        for short_term in short_terms:
            short_column = get_column_name(ma_type, short_term)
            for mid_term in mid_terms:
                mid_row = get_column_name(ma_type, mid_term)
                ma_return = get_sma_return(ticker.dd_data, short_term, mid_term, ma_type)
                df_ma_returns.at[mid_row, short_column] = round(ma_return, 3)
        ma_heatmap.heatmap_dataframes.append(df_ma_returns)
        ma_heatmap.instrument_symbols.append(instrument)
    heatmap_name = ma_type + "_all_heatmap.png"
    ma_heatmap.draw(heatmap_name)

def macd(instruments, macd_line_low = 16, macd_line_high = 26, macd_signal = 9):
    macd_df = pd.DataFrame()
    macd_df.set_axis(['MACD Avg. Returns'])
    for symbol in instruments:
        investments = classInv.Investments()
        fig_macd = classFigure.Fig_MACD()
        fig_name = "MACD_" + symbol + ".png"
        ticker = classTick.Tick(symbol)
        ticker.tick_macd(macd_line_low, macd_line_high, macd_signal)
        investments = trade_macd(ticker.dd_data, investments, ticker.symbol)
        fig_macd.tickers.append(ticker)
        for investment in investments.investments:
            fig_macd.trade_dates.append(investment.sell_date)
            fig_macd.trade_returns.append(investment.investment_return)
        fig_macd.draw(fig_name)
        investments.average_return()
        macd_df.at[0, symbol] = round(investments.investments_average_return, 3)
    common.df_to_tex(macd_df, "MACD.tex")
    print(macd_df)

def trade_macd(dd_data, investments, investment_symbol):
    investment = classInv.Investment()
    for data_row in range(len(dd_data) - 1):
        if dd_data.loc[data_row, "MACD"] > dd_data.loc[data_row, "MACD_SIGNAL"] and investment.buy_value == 0:
            investment = trade(dd_data, investment, investment_symbol, data_row, "buy")
        elif dd_data.loc[data_row, "MACD"] < dd_data.loc[data_row, "MACD_SIGNAL"] and investment.buy_value != 0:
            investments, investment = trade(dd_data, investment, investments, investment_symbol, data_row, "sell")
    return investments

def rsi(instruments, rsi_days = [9, 14, 28]):
    df_rsi = pd.DataFrame()
    df_rsi.set_axis(instruments)
    for symbol in instruments:
        ticker = classTick.Tick(symbol)
        fig_rsi = classFigure.Fig_RSI()
        fig_name = "RSI_" + symbol
        for period in rsi_days:
            investments = classInv.Investments()
            ticker.tick_rsi(period)
            investments = trade_rsi(ticker.dd_data, investments, ticker.symbol, period)
            investments.average_return()
            df_rsi.at[symbol, get_column_name("rsi", period)] = round(investments.investments_average_return, 3)
            fig_rsi.rsi_period.append(period)
            fig_rsi.tickers.append(ticker)
            buy_dates = []
            sell_dates = []
            trade_returns = []
            for investment in investments.investments:
                buy_dates.append(investment.buy_date)
                sell_dates.append(investment.sell_date)
                trade_returns.append(investment.investment_return)
            fig_rsi.sell_dates.append(sell_dates)
            fig_rsi.buy_dates.append(buy_dates)
            fig_rsi.trade_returns.append(trade_returns)
            fig_name = fig_name + "_" + get_column_name("rsi", period) + ".png"
        fig_rsi.draw(fig_name)
    common.df_to_tex(df_rsi, "RSI.tex")
    print(df_rsi)

def trade_rsi(dd_data, investments, investment_symbol, period):
    investment = classInv.Investment()
    rsi_column_name = get_column_name("rsi", period)
    buy = True
    for data_row in range(len(dd_data) - 1):
        if data_row > 0:
            prev_row = data_row - 1
        elif data_row == 0:
            prev_row = data_row
        if (
                (dd_data.loc[data_row, rsi_column_name] < 0.20 or
                (dd_data.loc[data_row, rsi_column_name] < 0.30 and dd_data.loc[prev_row, rsi_column_name] < dd_data.loc[data_row, rsi_column_name])) and
                buy
            ):
            investment = trade(dd_data, investment, investments, investment_symbol, data_row, "buy")
            buy = False
        elif (
                (dd_data.loc[data_row, rsi_column_name] > 0.80 or
                (dd_data.loc[data_row, rsi_column_name] > 0.70 and dd_data.loc[prev_row, rsi_column_name] > dd_data.loc[data_row, rsi_column_name])) and
                not buy
            ):
            investments, investment = trade(dd_data, investment, investments, investment_symbol, data_row, "sell")
            buy = True
    return investments

def roc(instruments, roc_days = [12, 25, 65, 130, 260]):
    df_roc = pd.DataFrame()
    df_roc.set_axis(instruments)
    for symbol in instruments:
        ticker = classTick.Tick(symbol)
        fig_roc = classFigure.Fig_ROC()
        fig_name = "ROC_" + symbol
        for period in roc_days:
            investments = classInv.Investments()
            ticker.tick_roc(period)
            investments = trade_roc(ticker.dd_data, investments, ticker.symbol, period)
            investments.average_return()
            df_roc.at[symbol, get_column_name("roc", period)] = round(investments.investments_average_return, 3)
            fig_roc.roc_period.append(period)
            fig_roc.tickers.append(ticker)
            buy_dates = []
            sell_dates = []
            trade_returns = []
            for investment in investments.investments:
                buy_dates.append(investment.buy_date)
                sell_dates.append(investment.sell_date)
                trade_returns.append(investment.investment_return)
            fig_roc.sell_dates.append(sell_dates)
            fig_roc.buy_dates.append(buy_dates)
            fig_roc.trade_returns.append(trade_returns)
            fig_name = fig_name + "_" + get_column_name("roc", period) + ".png"
        fig_roc.draw(fig_name)
    common.df_to_tex(df_roc, "ROC.tex")
    print(df_roc)

def trade_roc(dd_data, investments, investment_symbol, period):
    investment = classInv.Investment()
    roc_column_name = get_column_name("roc", period)
    max_value = dd_data[roc_column_name].max()
    min_value = dd_data[roc_column_name].min()
    upper_limit = ((max_value - 0.5) * 0.7) + 0.5
    lower_limit = 0.5 - ((0.5 - min_value) * 0.7)
    buy = True
    for data_row in range(len(dd_data) - 1):
        if dd_data.loc[data_row, roc_column_name] is not None:
            if (
                    dd_data.loc[data_row, roc_column_name] < lower_limit and buy
                ):
                investment = trade(dd_data, investment, investments, investment_symbol, data_row, "buy")
                buy = False
            elif (
                    dd_data.loc[data_row, roc_column_name] > upper_limit and not buy
                ):
                investments, investment = trade(dd_data, investment, investments, investment_symbol, data_row, "sell")
                buy = True
    return investments

def stoch(instruments, stoch_periods = [14, 3]):
    df_stoch = pd.DataFrame()
    df_stoch.set_axis(instruments)
    for symbol in instruments:
        ticker = classTick.Tick(symbol)
        fig_stoch = classFigure.Fig_Stoch()
        fig_name = "Stoch_" + symbol
        for periods in stoch_periods:
            investments = classInv.Investments()
            ticker.tick_stoch(periods)
            investments = trade_stoch(ticker.dd_data, investments, ticker.symbol, periods[0])
            investments.average_return()
            df_stoch.at[symbol, str(periods[0])] = round(investments.investments_average_return, 3)
            fig_stoch.stoch_period.append(periods)
            fig_stoch.tickers.append(ticker)
            buy_dates = []
            sell_dates = []
            trade_returns = []
            for investment in investments.investments:
                buy_dates.append(investment.buy_date)
                sell_dates.append(investment.sell_date)
                trade_returns.append(investment.investment_return)
            fig_stoch.sell_dates.append(sell_dates)
            fig_stoch.buy_dates.append(buy_dates)
            fig_stoch.trade_returns.append(trade_returns)
            fig_name = fig_name + "_" + str(stoch_periods[0]) + ".png"
        fig_stoch.draw(fig_name)
    common.df_to_tex(df_stoch, "Stoch.tex")
    print(df_stoch)

def trade_stoch(dd_data, investments, investment_symbol, period):
    investment = classInv.Investment()
    k_column_name = get_column_name("stoch_K", period)
    d_column_name = get_column_name("stoch_D", period)
    buy = True
    for data_row in range(len(dd_data) - 1):
        if data_row > 0:
            prev_row = data_row - 1
        elif data_row == 0:
            prev_row = data_row
        k_value = dd_data.loc[data_row, k_column_name]
        d_value = dd_data.loc[data_row, d_column_name]
        k_prev_value = dd_data.loc[prev_row, k_column_name]
        d_prev_value = dd_data.loc[prev_row, d_column_name]
        if math.isnan(k_value):
            k_value = 0
        if math.isnan(d_value):
            d_value = 0
        if math.isnan(k_prev_value):
            k_prev_value = 0
        if math.isnan(d_prev_value):
            d_prev_value = 0
        if buy:
            if (
                (k_value > 20 and k_value > k_prev_value) or
                (d_value > 20 and d_value > d_prev_value) or
                (k_value > d_value) or
                (d_value < 15)
            ):
                investment = trade(dd_data, investment, investments, investment_symbol, data_row, "buy")
                buy = False
        elif not buy:
            if (
                (k_value < 80 and k_value < k_prev_value) or
                (d_value < 80 and d_value < d_prev_value) or
                (k_value < d_value) or
                (d_value > 85)
            ):
                investments, investment = trade(dd_data, investment, investments, investment_symbol, data_row, "sell")
                buy = True
    return investments

def trade(dd_data, investment, investments, investment_symbol, data_row, trade_type):
    next_row = data_row + 1
    if trade_type == "buy":
        investment.symbol = investment_symbol
        investment.buy_date  = dd_data.loc[next_row, 'tick_date']
        investment.buy_value = dd_data.loc[next_row, 'price_opening']
        print(f'#### {investment_symbol} == {trade_type} == {investment.buy_date} == {investment.buy_value }')
        return investment
    elif trade_type == "sell":
        investment.sell_date = dd_data.loc[next_row, 'tick_date']
        investment.sell_value = dd_data.loc[next_row, 'price_opening']
        investment.sell()
        investments.add_investment(investment)
        print(f'#### {investment_symbol} == {trade_type} == {investment.sell_date} == {investment.sell_value }')
        investment = classInv.Investment()
        return investments, investment

def get_column_name(name_selector, name_data = ""):
    column_name = "NONE"
    match name_selector:
        case "SMA":
            column_name = "SMA-" + str(name_data)
        case "EMA":
            column_name = "EMA-" + str(name_data)
        case "rsi":
            column_name = "rsi-" + str(name_data)
        case "stoch_K":
            column_name = "stoch_K-" + str(name_data)
        case "stoch_D":
            column_name = "stoch_D-" + str(name_data)
    return column_name

if __name__ == "__main__":
    calc_ema = False
    calc_sma = False
    calc_macd = False
    calc_rsi = False
    calc_roc = False
    calc_stoch = True
    short_terms = [10, 15, 20, 25, 30]
    mid_terms = [50, 65, 100, 130, 200]
    rsi_days = [9, 14, 28]
    roc_days = [12, 25, 65, 130, 260]
    stoch_periods = [[14, 3], [80, 3], [280, 3]]
    instruments = []
    raw_instruments = list(db_query.get_all_tick_instrument())
    for instrument in raw_instruments:
        instruments.append(instrument[0])
    if calc_ema:
        moving_avrage(short_terms, mid_terms, instruments, "EMA")
    elif calc_sma:
        moving_avrage(short_terms, mid_terms, instruments, "SMA")
    elif calc_macd:
        macd(instruments)
    elif calc_rsi:
        rsi(instruments, rsi_days)
    elif calc_roc:
        roc(instruments, roc_days)
    elif calc_stoch:
        stoch(instruments, stoch_periods)