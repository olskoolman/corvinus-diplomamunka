import requests_alphaventage as alphaventage
import requests_eodhd as eodhd
import requests_yahoo as yahoo
from datetime import datetime
import db_queries as db_query

def get_data(instrumen_symbols, data_source, data_type):
    match data_source:
        case "alphaventage":
            alphaventage.stock_data(instrumen_symbols, data_type)
        case "eodhd":
            eodhd.stock_data(instrumen_symbols, data_type)
        case "yahoo":
            yahoo.stock_data(instrumen_symbols, data_type)

def check_existing_data(existing_data, check_value):
    check_value = datetime.strftime(check_value, '%Y-%m-%d')
    for row_data in existing_data:
        for date_value in row_data:
            if date_value == check_value:
                return True
    return False

if __name__ == "__main__":
    proc_tick_data = True
    proc_fundamental_data = False
    proc_fx_data = False
    proc_cmdt_data = False
    data_sources = db_query.get_all_data_source()
    for source in data_sources:
        instrument_symbol = source[0]
        source_symbol = source[1]
        data_source = source[2]
        data_type = source[3]
        stock_type = source[4]
        instrumen_symbols = [source_symbol, instrument_symbol]
        if data_type == 'TICK' and stock_type == 'STOCK' and proc_tick_data:
            print(f"====> {data_source} get daily {stock_type} data {instrument_symbol}")
            get_data(instrumen_symbols, data_source, stock_type)
        elif data_type == 'FUNDMNT' and stock_type == 'STOCK' and proc_fundamental_data:
            print(f'====> {data_source} get fundamental data {instrument_symbol}')
            get_data(instrumen_symbols, data_source, data_type)
        elif data_type == 'TICK' and stock_type == 'FX' and proc_fx_data:
            print(f'====> {data_source} get FX data {instrument_symbol}')
            get_data(instrumen_symbols, data_source, stock_type)
        elif data_type == 'TICK' and stock_type == 'CMDT' and proc_cmdt_data:
            print(f'====> {data_source} get CMDT data {instrument_symbol}')
            get_data(instrumen_symbols, data_source, stock_type)
        # else:
            # print(f"===> No data to process! {source}")