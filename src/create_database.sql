CREATE TABLE IF NOT EXISTS stock_type (
    stock_type_id SERIAL,
    stock_type_name VARCHAR(255),
    short_name VARCHAR(8) UNIQUE NOT NULL CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    PRIMARY KEY (stock_type_id)
);

CREATE TABLE IF NOT EXISTS industry (
    industry_id SERIAL,
    industry_name VARCHAR(255) UNIQUE NOT NULL,
    short_name VARCHAR(24) UNIQUE CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    PRIMARY KEY (industry_id)
);

CREATE TABLE IF NOT EXISTS fundamental_data_type (
    fundamental_data_type_id SERIAL,
    data_type_name VARCHAR(255) UNIQUE NOT NULL,
    short_name VARCHAR(24) UNIQUE CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    PRIMARY KEY (fundamental_data_type_id)
);

CREATE TABLE IF NOT EXISTS data_type (
    data_id SERIAL,
    data_type_name VARCHAR(255) UNIQUE NOT NULL,
    short_name VARCHAR(24) UNIQUE CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    PRIMARY KEY (data_id)
);

CREATE TABLE IF NOT EXISTS time_frame (
    time_id SERIAL,
    time_name VARCHAR(16),
    short_name VARCHAR(8) UNIQUE NOT NULL CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    PRIMARY KEY (time_id)
);

CREATE TABLE IF NOT EXISTS instrument (
    instrument_id SERIAL,
    instrument_name VARCHAR(255) UNIQUE NOT NULL,
    short_name VARCHAR(16) UNIQUE NOT NULL CHECK(short_name ~ '^[a-zA-Z0-9.=]*$'),
    stock_type VARCHAR(32) NOT NULL,
    industry VARCHAR(24),
    currency VARCHAR(8) NOT NULL,
    PRIMARY KEY (instrument_id),
    FOREIGN KEY (stock_type) REFERENCES stock_type (short_name),
    FOREIGN KEY (industry) REFERENCES industry (short_name)
);

CREATE TABLE IF NOT EXISTS fundamental_data (
    fundamental_data_id SERIAL,
    data_date DATE NOT NULL,
    data_type VARCHAR(24),
    data_value NUMERIC(32,4),
    instrument VARCHAR(16),
    PRIMARY KEY (fundamental_data_id),
    FOREIGN KEY (data_type) REFERENCES fundamental_data_type (short_name),
    FOREIGN KEY (instrument) REFERENCES instrument (short_name)
);

CREATE TABLE IF NOT EXISTS tick_data (
    tick_id BIGSERIAL,
    time_frame VARCHAR(8),
    price_low NUMERIC(16,4),
    price_high NUMERIC(16,4),
    price_opening NUMERIC(16,4),
    price_closing NUMERIC(16,4),
    instrument VARCHAR(16),
    volume BIGINT,
    tick_date DATE,
    PRIMARY KEY (tick_id),
    FOREIGN KEY (time_frame) REFERENCES time_frame(short_name),
    FOREIGN KEY (instrument) REFERENCES instrument(short_name)
);

CREATE TABLE IF NOT EXISTS data_source (
    source_id SERIAL,
    source_name VARCHAR(16),
    data_type VARCHAR(16),
    instrument_symbol VARCHAR(16) NOT NULL,
    source_symbol VARCHAR(16) NOT NULL,
    first_historical_data DATE,
    PRIMARY KEY (source_id),
    FOREIGN KEY (instrument_symbol) REFERENCES instrument (short_name),
    FOREIGN KEY (data_type) REFERENCES data_type (short_name)
);

INSERT INTO stock_type (stock_type_name, short_name)
VALUES
    ('commodity market','CMDT'),
    ('forex', 'FX'),
    ('stock', 'STOCK');

INSERT INTO data_type (data_type_name, short_name)
VALUES
    ('Tick data','TICK'),
    ('Fundamental data', 'FUNDMNT');

INSERT INTO time_frame (time_name, short_name)
VALUES
    ('seconds','sec'),
    ('minutes','min'),
    ('hours', 'hh'),
    ('days', 'dd'),
    ('months', 'MM'),
    ('years', 'YY');

INSERT INTO fundamental_data_type (data_type_name, short_name)
VALUES
    ('Divident','DIV'),
    ('Earning per share','EPS'),
    ('Earnings','Earnings');

INSERT INTO instrument (instrument_name, short_name, stock_type, currency)
VALUES
    ('Copper', 'COPPER', 'CMDT', 'USD'),
    ('Oil', 'BRENT', 'CMDT', 'USD'),
    ('Corn', 'CORN', 'CMDT', 'USD'),
    ('EUR/USD', 'EUR.USD', 'FX', 'USD'),
    ('EUR/HUF', 'EUR.HUF', 'FX', 'HUF'),
    ('MOL Nyrt.', 'MOL.BUD', 'STOCK', 'HUF'),
    ('OTP Bank Nyrt', 'OTP.BUD', 'STOCK', 'HUF'),
    ('Intel Corp', 'INTC', 'STOCK', 'USD'),
    ('Microsoft Corporation', 'MSFT', 'STOCK', 'USD'),
    ('Coca-Cola Company', 'KO', 'STOCK', 'USD');

INSERT INTO data_source (source_name, data_type, instrument_symbol, source_symbol)
VALUES
    ('yahoo', 'TICK', 'COPPER', 'HG=F'),
    ('yahoo', 'TICK', 'BRENT', 'BZ=F'),
    ('yahoo', 'TICK', 'CORN', 'ZC=F'),
    ('alphaventage', 'TICK', 'EUR.USD', 'EUR.USD'),
    ('alphaventage', 'TICK', 'EUR.HUF', 'EUR.HUF'),
    ('eodhd', 'TICK', 'MOL.BUD', 'MOL.BUD'),
    ('eodhd', 'TICK', 'OTP.BUD', 'OTP.BUD'),
    ('alphaventage', 'TICK', 'INTC', 'INTC'),
    ('alphaventage', 'TICK', 'MSFT', 'MSFT'),
    ('alphaventage', 'TICK', 'KO', 'KO'),
    ('yahoo', 'TICK', 'MOL.BUD', 'MOL.BUD'),
    ('yahoo', 'TICK', 'OTP.BUD', 'OTP.BUD'),
    ('alphaventage', 'FUNDMNT', 'INTC', 'INTC'),
    ('alphaventage', 'FUNDMNT', 'MSFT', 'MSFT'),
    ('alphaventage', 'FUNDMNT', 'KO', 'KO');